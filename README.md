## Distant Rendering for Wurm Unlimited

This Wurm Unlimited mod requires Ago's server modloader that can be found on GitHub: https://github.com/ago1024/WurmServerModLauncher/releases
The mod improves the distant terrain generation and calculates forests in the distance correctly.

This distant terrain generation is required by the distant rendering client mod that can be found here: https://gitlab.com/Dominikk/wu-sklotopolis-client-distantrendering-pub

To install this mod simply put all files into your mod folder of your Wurm Server, similar to every other Wurm Unlimited mod installation.

**You are using this mod at your own risk!**

Please report any found problems as a GitLab issue, I will look into them. In case you find any problem please include as many details as possible and screenshots of the problem.

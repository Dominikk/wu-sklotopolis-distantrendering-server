package com.sklotopolis.mods.server.distantrendering;

import com.wurmonline.mesh.MeshIO;
import com.wurmonline.mesh.Tiles;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import org.gotti.wurmunlimited.modloader.callbacks.CallbackApi;
import org.gotti.wurmunlimited.modloader.classhooks.HookException;
import org.gotti.wurmunlimited.modloader.classhooks.HookManager;
import org.gotti.wurmunlimited.modloader.interfaces.PreInitable;
import org.gotti.wurmunlimited.modloader.interfaces.WurmServerMod;

public class DistantRenderingServerMod implements WurmServerMod, PreInitable {

    @Override
    public void preInit() {

        try
        {
            ClassPool classPool = ClassPool.getDefault();
            CtClass ctMeshIO = classPool.get("com.wurmonline.mesh.MeshIO");
            HookManager.getInstance().addCallback(ctMeshIO, "distantrenderer", this);
            ctMeshIO.getDeclaredMethod("calcDistantTerrain").setBody("{" +
                    "this.distantTerrainTypes = new byte[this.size * this.size / 256];" +
                    "distantrenderer.calcDistantTerrainForest(this);" +
                    "}");
        } catch (NotFoundException | CannotCompileException e)
        {
            throw new HookException("Could not find inject into calcDistantTerrain" + e.getMessage());
        }

    }

    @CallbackApi
    public void calcDistantTerrainForest(MeshIO meshIO) {
        for (int xT = 0; xT < meshIO.getSize() / 16; ++xT)
        {
            for (int yT = 0; yT < meshIO.getSize() / 16; ++yT)
            {
                final int[] counts = new int[256];
                int vegetationCounter = 0;
                int killerTreeCounter = 0;
                for (int x = xT * 16; x < xT * 16 + 16; ++x)
                {
                    for (int y = yT * 16; y < yT * 16 + 16; ++y)
                    {
                        final int type = Tiles.decodeType(meshIO.getTile(x, y)) & 0xFF;
                        Tiles.Tile tile = Tiles.getTile(type);
                        if (tile != null)
                        {
                            if (tile.isTree() || tile.isBush())
                            {
                                vegetationCounter++;
                            }
                            if (tile == Tiles.Tile.TILE_ENCHANTED_TREE_WILLOW || tile == Tiles.Tile.TILE_MYCELIUM_TREE_WILLOW || tile == Tiles.Tile.TILE_TREE_WILLOW
                                    || tile == Tiles.Tile.TILE_ENCHANTED_TREE_OAK || tile == Tiles.Tile.TILE_MYCELIUM_TREE_OAK || tile == Tiles.Tile.TILE_TREE_OAK)
                            {
                                killerTreeCounter++;
                            }
                        }
                        ++counts[type];
                    }
                }
                int mostCommon = 0;
                for (int i = 0; i < 256; ++i)
                {
                    if (counts[i] > counts[mostCommon])
                    {
                        mostCommon = i;
                    }
                }

                Tiles.Tile mostCommonTile = Tiles.getTile(mostCommon);
                if (mostCommonTile != null && mostCommonTile.isGrass() && (vegetationCounter >= 50 || killerTreeCounter >= 9))
                {
                    mostCommon = 255;
                    //Find most common vegetation
                    for (int i = 0; i < 256; ++i)
                    {
                        Tiles.Tile tile = Tiles.getTile(i);
                        if (tile != null && (tile.isTree() || tile.isBush()))
                        {
                            if (counts[i] > counts[mostCommon])
                            {
                                mostCommon = i;
                            }
                        }
                    }
                }
                meshIO.getDistantTerrainTypes()[xT + yT * (meshIO.getSize() / 16)] = (byte) mostCommon;
            }
        }
    }
}
